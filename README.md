# Description #

This is the code repository for journal article _Topic Clustering and Classification on Final Project Reports: a Comparison of Traditional and Modern Approaches_ by Hendra Bunyamin, Heriyanto, Stevani Novianti, and Lisan Sulistiani.     

The link for the dataset is [here](https://maranathaedu-my.sharepoint.com/:u:/g/personal/hendra_bunyamin_it_maranatha_edu/ER_g9g6qs6BAknBBykc0bmYBSttIJ8naPtkdXkx1EyOJtA).

The link for the paper is [here](http://www.iaeng.org/IJCS/issues_v46/issue_3/IJCS_46_3_15.pdf).        

If you have questions, please feel free to ask by sending e-mails to **hendra.bunyamin@it.maranatha.edu**.
